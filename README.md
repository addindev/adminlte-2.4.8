# AdminLTE 2.4.8

## Paket starter kit AdminLTE versi 2.4.8
Include assets :
- Core dist adminLTE
- Bootstrap v3.3.7
- Select2 
- Datatables
- jQuery v3.3.1
- Font Awesome 4.7.0
- sweetalert2-7.33.1
- jquery-loading-overlay-2.1.7

## Page
- index.html
- login.html
> by : akhul syaifudin
